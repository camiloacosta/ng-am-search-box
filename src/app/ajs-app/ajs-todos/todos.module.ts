import * as ng from 'angular';
import ngSanitize from 'ng-sanitize';
import {moduleAmSearchBox} from 'am-search-box';
import {todosComponent} from './todos.component';

declare var angular: ng.IAngularStatic;

export const todosModule = angular.module('todos', ['ngSanitize', moduleAmSearchBox])
  .component('todos', todosComponent);
