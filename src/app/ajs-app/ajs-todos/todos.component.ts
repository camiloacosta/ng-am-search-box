import * as ng from 'angular';

class ComponentController implements ng.IComponentController {
}

export const todosComponent: ng.IComponentOptions = {
  controller: ComponentController,
  bindings: {
    onClick: '&',
    brand: '@',
    channel: '@',
    country: '@',
    lang: '@',
  },
  template: `
    <button ng-click="$ctrl.onClick('prueba')">Click me!</button>
    <am-search-box brand="{{$ctrl.brand}}" channel="{{$ctrl.channel}}" country="{{$ctrl.country}}" locale="{{$ctrl.lang}}"
      on-add-product="$ctrl.onAddProduct(name)" select-product="disney"></am-search-box>
  `,
};
