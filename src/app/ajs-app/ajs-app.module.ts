import * as ng from 'angular';
import {downgradeComponent} from '@angular/upgrade/static';
import {AppComponent} from '../app.component';
import {todosModule} from './ajs-todos/todos.module';
import {ajsRootComponent} from './ajs-root.component';

declare var angular: ng.IAngularStatic;

export const ajsAppModule = angular.module('ajsApp', [todosModule.name]);

ajsAppModule.component('ajsRoot', ajsRootComponent);
ajsAppModule.directive('appRoot', downgradeComponent({component: AppComponent}));

