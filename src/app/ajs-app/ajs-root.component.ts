import * as ng from 'angular';

export const ajsRootComponent: ng.IComponentOptions = {
  template: '<app-root></app-root>',
};
