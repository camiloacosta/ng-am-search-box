import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgTodosWrapperDirective } from './ng-todos-wrapper.directive';
import { NgTodoComponent } from './ng-todo/ng-todo.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [NgTodosWrapperDirective, NgTodoComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [NgTodosWrapperDirective, NgTodoComponent]
})
export class NgTodosModule { }
