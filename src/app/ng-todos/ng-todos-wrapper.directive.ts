import {Directive, ElementRef, Inject, Injector, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {UpgradeComponent} from '@angular/upgrade/static';

@Directive({
  selector: 'todos',
})
export class NgTodosWrapperDirective extends UpgradeComponent {

  @Input() brand: string;

  @Input() channel: string;

  @Input() country: string;

  @Input() lang: string;

  @Output() onClick;

  constructor(@Inject(ElementRef) elementRef: ElementRef, @Inject(Injector) injector: Injector) {
    super('todos', elementRef, injector);
  }

}
