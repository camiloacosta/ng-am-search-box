import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-ng-todo',
  templateUrl: './ng-todo.component.html',
  styleUrls: ['./ng-todo.component.scss'],
})
export class NgTodoComponent implements OnInit {

  brand = 'almundo';

  channel = 'web';

  country = 'col';

  language = 'es-CO';

  constructor() { }

  onTodoClick(event) {
    console.log('click from app component', event);
  }

}
