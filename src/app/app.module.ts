import {BrowserModule} from '@angular/platform-browser';
import {Inject, NgModule} from '@angular/core';
import {UpgradeModule} from '@angular/upgrade/static';

import {ajsAppModule} from './ajs-app/ajs-app.module';
import {AppComponent} from './app.component';
import { NgTodosModule } from './ng-todos/ng-todos.module';

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        UpgradeModule,
        NgTodosModule
    ],
    providers: [],
    bootstrap: [],
    entryComponents: [AppComponent],
})
export class AppModule {

    constructor(@Inject(UpgradeModule) public upgrade: UpgradeModule) {}

    ngDoBootstrap() {
        this.upgrade.bootstrap(document.body, [ajsAppModule.name]);
    }
}
